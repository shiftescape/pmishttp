# PMIS Http Service - v 0.1

### Installation ###
```sh
npm install --save git+https://shiftescape@bitbucket.org/shiftescape/pmishttp.git
```
### Usage ###
```sh
import { PMISHttpService } from 'pmishttp';
```

And inject the service to the constructor:
```sh
constructor (private PMISHttp: PMISHttpService) { ...
```
And you can use all request type provided: **GET**, **POST**, **PATCH**, **PUT**, **DELETE**
```sh
PMISHttp.get('/path-to-api-endpoint-or-file').subscribe((successCallback) => {
    // do something
}, (errorCallback) => {
    // do something
});
```
### Adding custom headers ###
```sh
PMISHttp.addHeaders({<key>, <value>});
```

### Note ###
By default, `'Accept': 'application/json'` and `'Content-Type': 'application/json'` headers are already added. Also, the `Authorization` header is being appended **every request** with  a value from `localStorage` item `token`.